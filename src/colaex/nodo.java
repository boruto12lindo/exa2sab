package colaex;

/**
 * @author Melina Karen Ticra Aguilar 
 * CI: 15208024
 */
public class nodo <T>{
    private T valor;
    private nodo<T> sigNod;
    
    public nodo(T valor, nodo<T> sigNod){
        this.valor=valor;
        this.sigNod=sigNod;
    }

    public T getValor() {
        return valor;
    }

    public void setValor(T valor) {
        this.valor = valor;
    }

    public nodo<T> getSigNod() {
        return sigNod;
    }

    public void setSigNod(nodo<T> sigNod) {
        this.sigNod = sigNod;
    }
}
