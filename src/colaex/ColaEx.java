package colaex;
import java.util.*;
/**
 * @author Melina Karen Ticra Aguilar 
 * CI: 15208024
 */
public class ColaEx {
        public static void main(String[] args) {
           /* Cola<Integer> cd= new Cola<>();
            System.out.println("encolamos ");
            System.out.println("La cola esta vacia? :"+cd.isEmpty());
            cd.enQueue(0);
            cd.enQueue(2);
            cd.enQueue(3);
            cd.enQueue(0);
            cd.enQueue(5);
            
            System.out.println("quien esta al frente es :"+cd.firstElement());
            System.out.println("La cola esta vacia? despues :"+cd.full());*/


        Scanner x = new Scanner(System. in);
        Cola col;
        operaciones oper;
        int  uno,dos,op;
        int yes = 0;
        System.out.println("Que tipo de cola se va a crear?");
        System.out.println("1. Dinámica");
        System.out.println("2. Limite");
        uno = x.nextInt();
        
        System.out.println("Tamanio de la cola");
        dos=x.nextInt();
        col=typeCola(uno,dos);
        oper=operacionCola(uno,col,dos);
        while(yes==0&&col!= null){
            System.out.println("1. Aniadir elementos");    
            System.out.println("2. Borrar un elemento de la estructura cola");
            System.out.println("3. Cambiar el límite de la cola");
            System.out.println("4. Mostrar el número de elementos que existe en la estructura cola");
            System.out.println("5. maximo y minimo");
            System.out.println("6. Buscar elementos");
            System.out.println("7. Ver todos los elementos");
            System.out.println("8. Ordenar elementos");
            System.out.println("9. Mostrar todos los elementos que tiene la cola con su tipo de dato");
            System.out.println("10. Sacar todos los elementos");
            System.out.println("n. salir");
            System.out.println("Que desea realizar");
            op = x.nextInt();
            switch(op){
                case 1:
                    System.out.println("Que valores desea ingresar");        
                    int count = 1;
                    while(count <= dos && !col.full()){
                         String vs = x.next();
                         if(uno == 1){
                             String vs_string = vs;
                             oper.insert(vs_string);
                         }else if(uno == 2){
                             int vs_integer = Integer.parseInt(vs);
                             oper.insert(vs_integer);
                         }else if(uno == 3){
                             boolean numeric = true;
                             numeric = vs.matches("-?\\d+(\\.\\d+)?");
                             if(numeric){
                                 int vbi_object = Integer.parseInt(vs);
                                 oper.insert(vbi_object);
                             }else{
                                 String vbs_object = vs;
                                 oper.insert(vbs_object);
                             }
                         }
                    }
                break;
                case 2:
                    oper.remove();
                break;
                case 4:
                    oper.occupiedFree();
                break;
                case 5:
                    oper.maxMin();
                break;
                case 6:
                    System.out.println("Que desea buscar");
                    String s = x.next();
                    oper.searchCola(s);
                break;
                case 7:
                    System.out.println("ordenando los datos");
                    oper.sortStack();// 1 5 2 4 3
                break;
                case 8:
                    System.out.println("todos los datos");
                    oper.showAll();
                break;
            }
            System.out.println("Desea continuar? precione 0 para si o cualquier otro para finalizar");
            yes = x.nextInt();
        }
        
    }
    
    public static Cola typeCola(int type, int size){
        Cola c;
        switch(type){
            case 1:
                Cola<String> c_string = new Cola<>();
                c = c_string;
                System.out.println("Cola String");
            break;
            case 2:
                Cola<Integer> c_integer= new Cola<>();
                c = c_integer;
                System.out.println("Cola Integer");
            break;
            default:
                Cola<Object> c_object = new Cola<>();
                c = c_object;
                System.out.println("Cola cualquier tipo");
            break;
        }
        return c;
    }
    
    public static operaciones operacionCola(int type,Cola p,int size){
        operaciones o;
        switch(type){
            case 1:
                operaciones<String> o_string = new operaciones<>(p,size);
                o = o_string;
            break;
            case 2:
                operaciones<Integer> o_integer = new operaciones<>(p,size);
                o = o_integer;                
            break;
            default:
                operaciones<Object> o_object = new operaciones<>(p,size);
                o = o_object;
            break;
        }
        return o;
    }
    
}
